[@ticmakers-react-native/button](../README.md) > ["styles"](../modules/_styles_.md)

# External module: "styles"

## Index

### Variables

* [styles](_styles_.md#styles)
* [useMaterialColors](_styles_.md#usematerialcolors)

### Object literals

* [colors](_styles_.md#colors)
* [materialColors](_styles_.md#materialcolors)

---

## Variables

<a id="styles"></a>

### `<Const>` styles

**● styles**: *`object`* =  StyleSheet.create({
  bgDanger: {
    backgroundColor: useMaterialColors ? materialColors.danger : colors.danger,
  },
  bgDark: {
    backgroundColor: useMaterialColors ? materialColors.dark : colors.dark,
  },
  bgInfo: {
    backgroundColor: useMaterialColors ? materialColors.info : colors.info,
  },
  bgLight: {
    backgroundColor: useMaterialColors ? materialColors.light : colors.light,
  },
  bgPrimary: {
    backgroundColor: useMaterialColors ? materialColors.primary : colors.primary,
  },
  bgSuccess: {
    backgroundColor: useMaterialColors ? materialColors.success : colors.success,
  },
  bgWarning: {
    backgroundColor: useMaterialColors ? materialColors.warning : colors.warning,
  },

  borderDanger: {
    borderColor: useMaterialColors ? materialColors.danger : colors.danger,
  },
  borderDark: {
    borderColor: useMaterialColors ? materialColors.dark : colors.dark,
  },
  borderInfo: {
    borderColor: useMaterialColors ? materialColors.info : colors.info,
  },
  borderLight: {
    borderColor: useMaterialColors ? materialColors.light : colors.light,
  },
  borderPrimary: {
    borderColor: useMaterialColors ? materialColors.primary : colors.primary,
  },
  borderSuccess: {
    borderColor: useMaterialColors ? materialColors.success : colors.success,
  },
  borderWarning: {
    borderColor: useMaterialColors ? materialColors.warning : colors.warning,
  },

  colorDanger: {
    color: useMaterialColors ? materialColors.danger : colors.danger,
  },
  colorDark: {
    color: useMaterialColors ? materialColors.dark : colors.dark,
  },
  colorInfo: {
    color: useMaterialColors ? materialColors.info : colors.info,
  },
  colorLight: {
    color: useMaterialColors ? materialColors.light : colors.light,
  },
  colorPrimary: {
    color: useMaterialColors ? materialColors.primary : colors.primary,
  },
  colorSuccess: {
    color: useMaterialColors ? materialColors.success : colors.success,
  },
  colorWarning: {
    color: useMaterialColors ? materialColors.warning : colors.warning,
  },

  titleStyle: {
  },

  btnLink: {
    height: 'auto',
    paddingBottom: 1,
    paddingTop: 0,
  },

  titleLink: {
    fontSize: 16,
    paddingLeft: 4,
    paddingRight: 4,
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
  },

  center: {
    alignSelf: 'center',
  },

  left: {
    alignSelf: 'flex-start',
  },

  right: {
    alignSelf: 'flex-end',
  },

})

*Defined in styles.ts:25*

#### Type declaration

 titleStyle: `object`

 bgDanger: `object`

 backgroundColor: `string`

 bgDark: `object`

 backgroundColor: `string`

 bgInfo: `object`

 backgroundColor: `string`

 bgLight: `object`

 backgroundColor: `string`

 bgPrimary: `object`

 backgroundColor: `string`

 bgSuccess: `object`

 backgroundColor: `string`

 bgWarning: `object`

 backgroundColor: `string`

 borderDanger: `object`

 borderColor: `string`

 borderDark: `object`

 borderColor: `string`

 borderInfo: `object`

 borderColor: `string`

 borderLight: `object`

 borderColor: `string`

 borderPrimary: `object`

 borderColor: `string`

 borderSuccess: `object`

 borderColor: `string`

 borderWarning: `object`

 borderColor: `string`

 btnLink: `object`

 height: `string`

 paddingBottom: `number`

 paddingTop: `number`

 center: `object`

 alignSelf: "center"

 colorDanger: `object`

 color: `string`

 colorDark: `object`

 color: `string`

 colorInfo: `object`

 color: `string`

 colorLight: `object`

 color: `string`

 colorPrimary: `object`

 color: `string`

 colorSuccess: `object`

 color: `string`

 colorWarning: `object`

 color: `string`

 left: `object`

 alignSelf: "flex-start"

 right: `object`

 alignSelf: "flex-end"

 titleLink: `object`

 fontSize: `number`

 paddingLeft: `number`

 paddingRight: `number`

 textDecorationLine: "underline"

 textDecorationStyle: "solid"

___
<a id="usematerialcolors"></a>

### `<Const>` useMaterialColors

**● useMaterialColors**: *`true`* = true

*Defined in styles.ts:23*

___

## Object literals

<a id="colors"></a>

### `<Const>` colors

**colors**: *`object`*

*Defined in styles.ts:3*

<a id="colors.danger"></a>

####  danger

**● danger**: *`string`* = "#ce3c3e"

*Defined in styles.ts:4*

___
<a id="colors.dark"></a>

####  dark

**● dark**: *`string`* = "#000000"

*Defined in styles.ts:5*

___
<a id="colors.info"></a>

####  info

**● info**: *`string`* = "#529ff3"

*Defined in styles.ts:6*

___
<a id="colors.light"></a>

####  light

**● light**: *`string`* = "#f1f1f1"

*Defined in styles.ts:7*

___
<a id="colors.primary"></a>

####  primary

**● primary**: *`string`* = "#0a60ff"

*Defined in styles.ts:8*

___
<a id="colors.success"></a>

####  success

**● success**: *`string`* = "#4dad4a"

*Defined in styles.ts:9*

___
<a id="colors.warning"></a>

####  warning

**● warning**: *`string`* = "#eb9e3e"

*Defined in styles.ts:10*

___

___
<a id="materialcolors"></a>

### `<Const>` materialColors

**materialColors**: *`object`*

*Defined in styles.ts:13*

<a id="materialcolors.danger-1"></a>

####  danger

**● danger**: *`string`* = "#d32f2f"

*Defined in styles.ts:14*

___
<a id="materialcolors.dark-1"></a>

####  dark

**● dark**: *`string`* =  colors.dark

*Defined in styles.ts:15*

___
<a id="materialcolors.info-1"></a>

####  info

**● info**: *`string`* = "#00acc1"

*Defined in styles.ts:16*

___
<a id="materialcolors.light-1"></a>

####  light

**● light**: *`string`* =  colors.light

*Defined in styles.ts:17*

___
<a id="materialcolors.primary-1"></a>

####  primary

**● primary**: *`string`* = "#1976d2"

*Defined in styles.ts:18*

___
<a id="materialcolors.success-1"></a>

####  success

**● success**: *`string`* = "#388e3c"

*Defined in styles.ts:19*

___
<a id="materialcolors.warning-1"></a>

####  warning

**● warning**: *`string`* = "#ffb300"

*Defined in styles.ts:20*

___

___

