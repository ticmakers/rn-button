[@ticmakers-react-native/button](../README.md) > ["index.d"](../modules/_index_d_.md)

# External module: "index.d"

## Index

### Classes

* [Button](../classes/_index_d_.button.md)

### Interfaces

* [IButtonProps](../interfaces/_index_d_.ibuttonprops.md)
* [IButtonState](../interfaces/_index_d_.ibuttonstate.md)
* [ITouchableOpacityProps](../interfaces/_index_d_.itouchableopacityprops.md)

---

