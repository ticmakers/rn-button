
TIC Makers - React Native Button
================================

React native component for button.

Powered by [TIC Makers](https://ticmakers.com)

Demo
----

Button Expo's snack

Install
-------

Install `@ticmakers-react-native/button` package and save into `package.json`:

NPM

```shell
$ npm install @ticmakers-react-native/button --save
```

Yarn

```shell
$ yarn add @ticmakers-react-native/button
```

How to use?
-----------

```javascript
import React from 'react'
import Button from '@ticmakers-react-native/button'
import Icon from '@ticmakers-react-native/icon'

export default class App extends React.Component {
  render() {
    return (
      <Button title="My button" iconLeft={{ name: 'star' }} />

      // OR

      <Button iconRight>
        <Text>My button</Text>
        <Icon name="star" />
      </Button>
    )
  }
}
```

Properties
----------

Name

Type

Default Value

Definition

title

\-

\-

\-

Todo
----

*   Test on iOS
*   Improve and add new features
*   Add more styles
*   Improve readme (example & demo)
*   Create tests

Version 1.0.5 ([Changelog](https://bitbucket.org/ticmakers/rn-button/src/master/CHANGELOG.md))
----------------------------------------------------------------------------------------------

## Index

### External modules

* ["Button"](modules/_button_.md)
* ["index"](modules/_index_.md)
* ["index.d"](modules/_index_d_.md)
* ["styles"](modules/_styles_.md)

---

