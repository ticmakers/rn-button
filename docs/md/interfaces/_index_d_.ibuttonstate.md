[@ticmakers-react-native/button](../README.md) > ["index.d"](../modules/_index_d_.md) > [IButtonState](../interfaces/_index_d_.ibuttonstate.md)

# Interface: IButtonState

Interface to define the state of the Button component

*__interface__*: IButtonState

## Hierarchy

**IButtonState**

## Index

---

