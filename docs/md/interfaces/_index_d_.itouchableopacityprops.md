[@ticmakers-react-native/button](../README.md) > ["index.d"](../modules/_index_d_.md) > [ITouchableOpacityProps](../interfaces/_index_d_.itouchableopacityprops.md)

# Interface: ITouchableOpacityProps

Interface to define the touchable opacity props of the Button component

*__interface__*: ITouchableOpacityProps

*__extends__*: {TouchableWithoutFeedbackProps}

*__platform__*: iOS

## Hierarchy

 `TouchableWithoutFeedbackProps`

**↳ ITouchableOpacityProps**

## Index

### Properties

* [accessibilityActions](_index_d_.itouchableopacityprops.md#accessibilityactions)
* [accessibilityComponentType](_index_d_.itouchableopacityprops.md#accessibilitycomponenttype)
* [accessibilityElementsHidden](_index_d_.itouchableopacityprops.md#accessibilityelementshidden)
* [accessibilityHint](_index_d_.itouchableopacityprops.md#accessibilityhint)
* [accessibilityIgnoresInvertColors](_index_d_.itouchableopacityprops.md#accessibilityignoresinvertcolors)
* [accessibilityLabel](_index_d_.itouchableopacityprops.md#accessibilitylabel)
* [accessibilityLiveRegion](_index_d_.itouchableopacityprops.md#accessibilityliveregion)
* [accessibilityRole](_index_d_.itouchableopacityprops.md#accessibilityrole)
* [accessibilityState](_index_d_.itouchableopacityprops.md#accessibilitystate)
* [accessibilityStates](_index_d_.itouchableopacityprops.md#accessibilitystates)
* [accessibilityTraits](_index_d_.itouchableopacityprops.md#accessibilitytraits)
* [accessibilityViewIsModal](_index_d_.itouchableopacityprops.md#accessibilityviewismodal)
* [accessible](_index_d_.itouchableopacityprops.md#accessible)
* [activityOpacity](_index_d_.itouchableopacityprops.md#activityopacity)
* [delayLongPress](_index_d_.itouchableopacityprops.md#delaylongpress)
* [delayPressIn](_index_d_.itouchableopacityprops.md#delaypressin)
* [delayPressOut](_index_d_.itouchableopacityprops.md#delaypressout)
* [disabled](_index_d_.itouchableopacityprops.md#disabled)
* [hasTVPreferredFocus](_index_d_.itouchableopacityprops.md#hastvpreferredfocus)
* [hitSlop](_index_d_.itouchableopacityprops.md#hitslop)
* [importantForAccessibility](_index_d_.itouchableopacityprops.md#importantforaccessibility)
* [onAccessibilityAction](_index_d_.itouchableopacityprops.md#onaccessibilityaction)
* [onAccessibilityTap](_index_d_.itouchableopacityprops.md#onaccessibilitytap)
* [onBlur](_index_d_.itouchableopacityprops.md#onblur)
* [onFocus](_index_d_.itouchableopacityprops.md#onfocus)
* [onLayout](_index_d_.itouchableopacityprops.md#onlayout)
* [onLongPress](_index_d_.itouchableopacityprops.md#onlongpress)
* [onMagicTap](_index_d_.itouchableopacityprops.md#onmagictap)
* [onPress](_index_d_.itouchableopacityprops.md#onpress)
* [onPressIn](_index_d_.itouchableopacityprops.md#onpressin)
* [onPressOut](_index_d_.itouchableopacityprops.md#onpressout)
* [pressRetentionOffset](_index_d_.itouchableopacityprops.md#pressretentionoffset)
* [style](_index_d_.itouchableopacityprops.md#style)
* [testID](_index_d_.itouchableopacityprops.md#testid)
* [tvParallaxProperties](_index_d_.itouchableopacityprops.md#tvparallaxproperties)

---

## Properties

<a id="accessibilityactions"></a>

### `<Optional>` accessibilityActions

**● accessibilityActions**: *`ReadonlyArray`<`AccessibilityActionInfo`>*

*Inherited from AccessibilityProps.accessibilityActions*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:1978*

Provides an array of custom actions available for accessibility.

___
<a id="accessibilitycomponenttype"></a>

### `<Optional>` accessibilityComponentType

**● accessibilityComponentType**: *"none" \| "button" \| "radiobutton_checked" \| "radiobutton_unchecked"*

*Inherited from AccessibilityPropsAndroid.accessibilityComponentType*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:2092*

In some cases, we also want to alert the end user of the type of selected component (i.e., that it is a “button”). If we were using native buttons, this would work automatically. Since we are using javascript, we need to provide a bit more context for TalkBack. To do so, you must specify the ‘accessibilityComponentType’ property for any UI component. For instances, we support ‘button’, ‘radiobutton\_checked’ and ‘radiobutton\_unchecked’ and so on.

*__platform__*: android

___
<a id="accessibilityelementshidden"></a>

### `<Optional>` accessibilityElementsHidden

**● accessibilityElementsHidden**: *`undefined` \| `false` \| `true`*

*Inherited from AccessibilityPropsIOS.accessibilityElementsHidden*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:2122*

A Boolean value indicating whether the accessibility elements contained within this accessibility element are hidden to the screen reader.

*__platform__*: ios

___
<a id="accessibilityhint"></a>

### `<Optional>` accessibilityHint

**● accessibilityHint**: *`undefined` \| `string`*

*Inherited from AccessibilityProps.accessibilityHint*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:2002*

An accessibility hint helps users understand what will happen when they perform an action on the accessibility element when that result is not obvious from the accessibility label.

___
<a id="accessibilityignoresinvertcolors"></a>

### `<Optional>` accessibilityIgnoresInvertColors

**● accessibilityIgnoresInvertColors**: *`undefined` \| `false` \| `true`*

*Inherited from AccessibilityPropsIOS.accessibilityIgnoresInvertColors*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:2153*

[https://facebook.github.io/react-native/docs/accessibility#accessibilityignoresinvertcolorsios](https://facebook.github.io/react-native/docs/accessibility#accessibilityignoresinvertcolorsios)

*__platform__*: ios

___
<a id="accessibilitylabel"></a>

### `<Optional>` accessibilityLabel

**● accessibilityLabel**: *`undefined` \| `string`*

*Inherited from AccessibilityProps.accessibilityLabel*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:1984*

Overrides the text that's read by the screen reader when the user interacts with the element. By default, the label is constructed by traversing all the children and accumulating all the Text nodes separated by space.

___
<a id="accessibilityliveregion"></a>

### `<Optional>` accessibilityLiveRegion

**● accessibilityLiveRegion**: *"none" \| "polite" \| "assertive"*

*Inherited from AccessibilityPropsAndroid.accessibilityLiveRegion*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:2100*

Indicates to accessibility services whether the user should be notified when this view changes. Works for Android API >= 19 only. See [http://developer.android.com/reference/android/view/View.html#attr\_android:accessibilityLiveRegion](http://developer.android.com/reference/android/view/View.html#attr_android:accessibilityLiveRegion) for references.

*__platform__*: android

___
<a id="accessibilityrole"></a>

### `<Optional>` accessibilityRole

**● accessibilityRole**: *`AccessibilityRole`*

*Inherited from AccessibilityProps.accessibilityRole*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:1989*

Accessibility Role tells a person using either VoiceOver on iOS or TalkBack on Android the type of element that is focused on.

___
<a id="accessibilitystate"></a>

### `<Optional>` accessibilityState

**● accessibilityState**: *`AccessibilityState`*

*Inherited from AccessibilityProps.accessibilityState*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:1998*

Accessibility State tells a person using either VoiceOver on iOS or TalkBack on Android the state of the element currently focused on.

___
<a id="accessibilitystates"></a>

### `<Optional>` accessibilityStates

**● accessibilityStates**: *`AccessibilityStates`[]*

*Inherited from AccessibilityProps.accessibilityStates*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:1994*

Accessibility State tells a person using either VoiceOver on iOS or TalkBack on Android the state of the element currently focused on.

*__deprecated:__*: accessibilityState available in 0.60+

___
<a id="accessibilitytraits"></a>

### `<Optional>` accessibilityTraits

**● accessibilityTraits**: *`AccessibilityTrait` \| `AccessibilityTrait`[]*

*Inherited from AccessibilityPropsIOS.accessibilityTraits*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:2129*

Accessibility traits tell a person using VoiceOver what kind of element they have selected. Is this element a label? A button? A header? These questions are answered by accessibilityTraits.

*__platform__*: ios

___
<a id="accessibilityviewismodal"></a>

### `<Optional>` accessibilityViewIsModal

**● accessibilityViewIsModal**: *`undefined` \| `false` \| `true`*

*Inherited from AccessibilityPropsIOS.accessibilityViewIsModal*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:2135*

A Boolean value indicating whether VoiceOver should ignore the elements within views that are siblings of the receiver.

*__platform__*: ios

___
<a id="accessible"></a>

### `<Optional>` accessible

**● accessible**: *`undefined` \| `false` \| `true`*

*Inherited from AccessibilityProps.accessible*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:1973*

When true, indicates that the view is an accessibility element. By default, all the touchable elements are accessible.

___
<a id="activityopacity"></a>

### `<Optional>` activityOpacity

**● activityOpacity**: *`undefined` \| `number`*

*Defined in index.d.ts:29*

Determines what the opacity of the wrapped view should be when touch is active

*__type__*: {number}

*__default__*: 0.2

___
<a id="delaylongpress"></a>

### `<Optional>` delayLongPress

**● delayLongPress**: *`undefined` \| `number`*

*Inherited from TouchableWithoutFeedbackProps.delayLongPress*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4715*

Delay in ms, from onPressIn, before onLongPress is called.

___
<a id="delaypressin"></a>

### `<Optional>` delayPressIn

**● delayPressIn**: *`undefined` \| `number`*

*Inherited from TouchableWithoutFeedbackProps.delayPressIn*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4720*

Delay in ms, from the start of the touch, before onPressIn is called.

___
<a id="delaypressout"></a>

### `<Optional>` delayPressOut

**● delayPressOut**: *`undefined` \| `number`*

*Inherited from TouchableWithoutFeedbackProps.delayPressOut*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4725*

Delay in ms, from the release of the touch, before onPressOut is called.

___
<a id="disabled"></a>

### `<Optional>` disabled

**● disabled**: *`undefined` \| `false` \| `true`*

*Inherited from TouchableWithoutFeedbackProps.disabled*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4730*

If true, disable all interactions for this component.

___
<a id="hastvpreferredfocus"></a>

### `<Optional>` hasTVPreferredFocus

**● hasTVPreferredFocus**: *`undefined` \| `false` \| `true`*

*Overrides TouchableWithoutFeedbackPropsIOS.hasTVPreferredFocus*

*Defined in index.d.ts:36*

(Apple TV only) TV preferred focus (see documentation for the View component)

*__type__*: {boolean}

*__platform__*: iOS

___
<a id="hitslop"></a>

### `<Optional>` hitSlop

**● hitSlop**: *`Insets`*

*Inherited from TouchableWithoutFeedbackProps.hitSlop*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4739*

This defines how far your touch can start away from the button. This is added to pressRetentionOffset when moving off of the button. NOTE The touch area never extends past the parent view bounds and the Z-index of sibling views always takes precedence if a touch hits two overlapping views.

___
<a id="importantforaccessibility"></a>

### `<Optional>` importantForAccessibility

**● importantForAccessibility**: *"auto" \| "yes" \| "no" \| "no-hide-descendants"*

*Inherited from AccessibilityPropsAndroid.importantForAccessibility*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:2113*

Controls how view is important for accessibility which is if it fires accessibility events and if it is reported to accessibility services that query the screen. Works for Android only. See [http://developer.android.com/reference/android/R.attr.html#importantForAccessibility](http://developer.android.com/reference/android/R.attr.html#importantForAccessibility) for references.

Possible values: 'auto' - The system determines whether the view is important for accessibility - default (recommended). 'yes' - The view is important for accessibility. 'no' - The view is not important for accessibility. 'no-hide-descendants' - The view is not important for accessibility, nor are any of its descendant views.

___
<a id="onaccessibilityaction"></a>

### `<Optional>` onAccessibilityAction

**● onAccessibilityAction**: *`undefined` \| `function`*

*Inherited from AccessibilityProps.onAccessibilityAction*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:2007*

When `accessible` is true, the system will try to invoke this function when the user performs an accessibility custom action.

___
<a id="onaccessibilitytap"></a>

### `<Optional>` onAccessibilityTap

**● onAccessibilityTap**: *`undefined` \| `function`*

*Inherited from AccessibilityPropsIOS.onAccessibilityTap*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:2141*

When `accessible` is true, the system will try to invoke this function when the user performs accessibility tap gesture.

*__platform__*: ios

___
<a id="onblur"></a>

### `<Optional>` onBlur

**● onBlur**: *`undefined` \| `function`*

*Inherited from TouchableWithoutFeedbackProps.onBlur*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4746*

When `accessible` is true (which is the default) this may be called when the OS-specific concept of "blur" occurs, meaning the element lost focus. Some platforms may not have the concept of blur.

___
<a id="onfocus"></a>

### `<Optional>` onFocus

**● onFocus**: *`undefined` \| `function`*

*Inherited from TouchableWithoutFeedbackProps.onFocus*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4753*

When `accessible` is true (which is the default) this may be called when the OS-specific concept of "focus" occurs. Some platforms may not have the concept of focus.

___
<a id="onlayout"></a>

### `<Optional>` onLayout

**● onLayout**: *`undefined` \| `function`*

*Inherited from TouchableWithoutFeedbackProps.onLayout*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4759*

Invoked on mount and layout changes with {nativeEvent: {layout: {x, y, width, height}}}

___
<a id="onlongpress"></a>

### `<Optional>` onLongPress

**● onLongPress**: *`undefined` \| `function`*

*Inherited from TouchableWithoutFeedbackProps.onLongPress*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4761*

___
<a id="onmagictap"></a>

### `<Optional>` onMagicTap

**● onMagicTap**: *`undefined` \| `function`*

*Inherited from AccessibilityPropsIOS.onMagicTap*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:2147*

When accessible is true, the system will invoke this function when the user performs the magic tap gesture.

*__platform__*: ios

___
<a id="onpress"></a>

### `<Optional>` onPress

**● onPress**: *`undefined` \| `function`*

*Inherited from TouchableWithoutFeedbackProps.onPress*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4767*

Called when the touch is released, but not if cancelled (e.g. by a scroll that steals the responder lock).

___
<a id="onpressin"></a>

### `<Optional>` onPressIn

**● onPressIn**: *`undefined` \| `function`*

*Inherited from TouchableWithoutFeedbackProps.onPressIn*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4769*

___
<a id="onpressout"></a>

### `<Optional>` onPressOut

**● onPressOut**: *`undefined` \| `function`*

*Inherited from TouchableWithoutFeedbackProps.onPressOut*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4771*

___
<a id="pressretentionoffset"></a>

### `<Optional>` pressRetentionOffset

**● pressRetentionOffset**: *`Insets`*

*Inherited from TouchableWithoutFeedbackProps.pressRetentionOffset*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4786*

When the scroll view is disabled, this defines how far your touch may move off of the button, before deactivating the button. Once deactivated, try moving it back and you'll see that the button is once again reactivated! Move it back and forth several times while the scroll view is disabled. Ensure you pass in a constant to reduce memory allocations.

___
<a id="style"></a>

### `<Optional>` style

**● style**: *`TypeStyle`*

*Overrides TouchableWithoutFeedbackProps.style*

*Defined in index.d.ts:42*

Apply a custom style

*__type__*: {TypeStyle}

___
<a id="testid"></a>

### `<Optional>` testID

**● testID**: *`undefined` \| `string`*

*Inherited from TouchableWithoutFeedbackProps.testID*

*Defined in /home/hackettyam/www/@ticmakers/react-native/Button/node_modules/@types/react-native/index.d.ts:4791*

Used to locate this view in end-to-end tests.

___
<a id="tvparallaxproperties"></a>

### `<Optional>` tvParallaxProperties

**● tvParallaxProperties**: *`undefined` \| `object`*

*Overrides TouchableWithoutFeedbackPropsIOS.tvParallaxProperties*

*Defined in index.d.ts:51*

(Apple TV only) Object with properties to control Apple TV parallax effects.

enabled: If true, parallax effects are enabled. Defaults to true. shiftDistanceX: Defaults to 2.0. shiftDistanceY: Defaults to 2.0. tiltAngle: Defaults to 0.05. magnification: Defaults to 1.0. pressMagnification: Defaults to 1.0. pressDuration: Defaults to 0.3. pressDelay: Defaults to 0.0.

*__type__*: {object}

*__platform__*: iOS

___

