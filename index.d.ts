import Button, { ITouchableOpacityProps, IButtonProps, IButtonState } from './src/index.d'

/**
 * Declare module
 */
declare module '@ticmakers-react-native/button'

/**
 * Export default
 */
export default Button

export {
  ITouchableOpacityProps,
  IButtonProps,
  IButtonState,
}
