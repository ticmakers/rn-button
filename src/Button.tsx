import * as React from 'react'
import { Button as NB_Button, Text } from 'native-base'
import * as tinycolor from 'tinycolor2'
import * as _ from 'lodash'
import {
  ActivityIndicator,
  BackgroundPropType,
  RippleBackgroundPropType,
  StyleSheet,
  TouchableNativeFeedback,
} from 'react-native'

import { AppHelper, TypeComponent, TypeStyle } from '@ticmakers-react-native/core'
import Icon, { IIconProps } from '@ticmakers-react-native/icon'
import { IButtonProps, IButtonState } from './index.d'
import styles from './styles'

/**
 * Class to define the component Button
 * @class Button
 * @extends {React.Component<IButtonProps, IButtonState>}
 */
export default class Button extends React.Component<IButtonProps, IButtonState> {
  /**
   * Creates an instance of Button.
   * @param {IButtonProps} props The props of the button
   */
  constructor(props: IButtonProps) {
    super(props)
    this.state = this._processProps()
    this._getBackgroundRipple()
  }

  /**
   * Method that renders the component
   * @returns {TypeComponent}
   */
  public render(): TypeComponent {
    const { clear, disabled, iconLeft, iconRight, link, loading } = this._processProps()

    const props = {
      ...this.props,
      ...this._processProps(),
      background: this._getBackgroundRipple(),
      center: undefined,
      disabled: disabled || loading,
      iconLeft: iconLeft as boolean,
      iconRight: iconRight as boolean,
      left: undefined,
      onPress: this._onPressAction.bind(this),
      right: undefined,
      style: this._processStyle(),
      transparent: clear || link,
    }

    return (
      <NB_Button { ...props }>
        { this.ButtonContent() }
      </NB_Button>
    )
  }

  /**
   * Method that return a array of components for the button content
   * @returns {TypeComponent[]}
   */
  public ButtonContent(): TypeComponent[] {
    const { hideIconLeft, hideIconRight, hideIcons, iconLeft, iconRight, large, loading, small, title } = this._processProps()

    if (loading) {
      const _props = {
        color: this._filterStyle('color', this._processStyle()),
        key: 'loading',
        size: (small || loading) ? 'small' : (large ? 'large' : 26) as 'small' | 'large' | number,
      }
      return [<ActivityIndicator { ..._props } />]
    }

    return ([
      (iconLeft && !hideIcons && !hideIconLeft) && this.IconLeft(),
      (this._getChildren() as any) || (title && this.Title()),
      (iconRight && !hideIcons && !hideIconRight) && this.IconRight(),
    ])
  }

  /**
   * Method thar return the title component
   * @returns {TypeComponent}
   */
  public Title(): TypeComponent {
    const { link, title, titleStyle } = this._processProps()
    const _color = (this._getStyleColor() as any).color
    const _style = { color: _color }

    return (
      <Text
        key={ title }
        style={ [styles.titleStyle, _style, link && styles.titleLink, titleStyle] }
        uppercase={ !link }
      >{ title }</Text>
    )
  }

  /**
   * Method that return the icon left component
   * @returns {TypeComponent}
   */
  public IconLeft(): TypeComponent {
    const { iconLeft } = this._processProps()
    let icon: TypeComponent

    const _style = this._getStyleColor()
    const _color = (_style as any).color

    if (iconLeft) {
      if (AppHelper.isComponent(iconLeft)) {
        const color = (iconLeft as any).props.color
        icon = React.cloneElement(iconLeft as any, { color: (color || _color) })
      } else if (this._isProps(iconLeft)) {
        icon = <Icon color={ _color } { ...iconLeft as IIconProps } key={0} />
      }
    }

    return icon
  }

  /**
   * Method that return the icon right component
   * @returns {TypeComponent}
   */
  public IconRight(): TypeComponent {
    const { iconRight } = this._processProps()
    let icon: TypeComponent

    const _style = this._getStyleColor()
    const _color = (_style as any).color

    if (iconRight) {
      if (AppHelper.isComponent(iconRight)) {
        const color = (iconRight as any).props.color
        icon = React.cloneElement(iconRight as any, { color: (color || _color) })
      } else if (this._isProps(iconRight)) {
        icon = <Icon color={ _color } { ...iconRight as IIconProps } key={1} />
      }
    }

    return icon
  }

  /**
   * Method that fire when the button is pressed
   * @private
   * @returns {void}
   */
  private _onPressAction(): void {
    const { onPress } = this._processProps()

    if (onPress) {
      return onPress()
    }
  }

  /**
   * Method that valid if a object is a props
   * @private
   * @param {*} kind Object to validate
   * @returns {boolean}
   */
  private _isProps(kind: any): boolean {
    return (!AppHelper.isComponent(kind) && Object.keys(kind).length > 0)
  }

  /**
   * Method that return the children components
   * @private
   * @returns {TypeComponent[]}
   */
  private _getChildren(): TypeComponent[] {
    const { hideIcons, hideIconLeft, hideIconRight, link } = this._processProps()
    const { children } = this.props
    const _style = this._getStyleColor()
    const _color = (_style as any).color

    return React.Children.map(children, (child: any, index: number) => {
      const isIcon = child.type === Icon
      const isText = child.type === Text
      const isIconLeft = (isIcon && index === 0)
      const isIconRight = (isIcon && index >= 1)
      const propsIcon = { color: _color, ...child.props }
      const propsText = { style: { color: _color }, ...child.props }

      if (link) {
        propsText.style = { ...propsText.style, ...styles.titleLink }
        propsText.uppercase = false
      }

      if (isIcon && !hideIcons) {
        if ((isIconLeft && !hideIconLeft) || (isIconRight && !hideIconRight)) {
          return React.cloneElement(child, propsIcon)
        }
      // tslint:disable-next-line: no-else-after-return
      } else if (!isIcon) {
        return React.cloneElement(child, isText ? propsText : child.props)
      }
    })
  }

  /**
   * Method that process the props of the button component
   * @private
   * @returns {IButtonProps}
   */
  private _processProps(): IButtonProps {
    const { active, backgroundRipple, block, bordered, center, clear, danger, dark, disabled, full, hideIconLeft, hideIconRight, hideIcons, iconLeft, iconRight, info, large, left, light, link, loading, onPress, primary, right, rounded, small, style, success, title, titleStyle, warning } = this.props

    const props: IButtonProps = {
      active: (typeof active !== 'undefined' ? active : false),
      backgroundRipple: (typeof backgroundRipple !== 'undefined' ? backgroundRipple : undefined),
      block: (typeof block !== 'undefined' ? block : false),
      bordered: (typeof bordered !== 'undefined' ? bordered : false),
      center: (typeof center !== 'undefined' ? center : false),
      clear: (typeof clear !== 'undefined' ? clear : false),
      danger: (typeof danger !== 'undefined' ? danger : false),
      dark: (typeof dark !== 'undefined' ? dark : false),
      disabled: (typeof disabled !== 'undefined' ? disabled : false),
      full: (typeof full !== 'undefined' ? full : false),
      hideIconLeft: (typeof hideIconLeft !== 'undefined' ? hideIconLeft : false),
      hideIconRight: (typeof hideIconRight !== 'undefined' ? hideIconRight : false),
      hideIcons: (typeof hideIcons !== 'undefined' ? hideIcons : false),
      iconLeft: (typeof iconLeft !== 'undefined' ? iconLeft : false),
      iconRight: (typeof iconRight !== 'undefined' ? iconRight : false),
      info: (typeof info !== 'undefined' ? info : false),
      large: (typeof large !== 'undefined' ? large : false),
      left: (typeof left !== 'undefined' ? left : false),
      light: (typeof light !== 'undefined' ? light : false),
      link: (typeof link !== 'undefined' ? link : false),
      loading: (typeof loading !== 'undefined' ? loading : false),
      onPress: (typeof onPress !== 'undefined' ? onPress : undefined),
      primary: (typeof primary !== 'undefined' ? primary : true),
      right: (typeof right !== 'undefined' ? right : false),
      rounded: (typeof rounded !== 'undefined' ? rounded : false),
      small: (typeof small !== 'undefined' ? small : false),
      style: (typeof style !== 'undefined' ? style : undefined),
      success: (typeof success !== 'undefined' ? success : false),
      title: (typeof title !== 'undefined' ? title : undefined),
      titleStyle: (typeof titleStyle !== 'undefined' ? titleStyle : undefined),
      warning: (typeof warning !== 'undefined' ? warning : false),
    }

    return props
  }

  /**
   * Method that process the style prop of the button component
   * @private
   * @returns {TypeStyle}
   */
  private _processStyle(): TypeStyle {
    const { center, iconLeft, iconRight, left, link, right, style } = this._processProps()
    let _style: TypeStyle = _.clone(style)
    let _newStyle: TypeStyle = {
      ...this._getStyleColor() as object,
    }

    if (iconLeft) { _newStyle.paddingLeft = 16 }
    if (iconRight) { _newStyle.paddingRight = 16 }

    if (center) { _newStyle.alignSelf = 'center' }
    if (left) { _newStyle.alignSelf = 'flex-start' }
    if (right) { _newStyle.alignSelf = 'flex-end' }

    if (Array.isArray(_style)) {
      _style.unshift(_newStyle)
      if (link) { _style.unshift(styles.btnLink) }
    } else if (typeof _style === 'object' && Object.keys(_style as object).length > 0) {
      _style = StyleSheet.flatten([_newStyle, _style])
      if (link) { _style = StyleSheet.flatten([styles.btnLink, _style]) }
    }

    if (!_style) { _newStyle = StyleSheet.flatten([_newStyle, styles.btnLink]) }

    return _style || _newStyle
  }

  /**
   * Method to generate the style theme for the button component
   * @private
   * @returns {TypeStyle}
   */
  private _getStyleColor(): TypeStyle {
    const { bordered, clear, danger, dark, info, light, link, success, warning } = this._processProps()
    let styleColor = { ...styles.bgPrimary, color: 'white' }

    if (bordered || clear || link) {
      styleColor = { ...styleColor, ...styles.colorPrimary }

      if (bordered) {
        styleColor = { ...styleColor, ...styles.borderPrimary }
      }
    }

    if (danger) {
      styleColor = { ...styleColor, ...styles.bgDanger }

      if (bordered || clear || link) {
        styleColor = { ...styleColor, ...styles.colorDanger }

        if (bordered) {
          styleColor = { ...styleColor, ...styles.borderDanger }
        }
      }
    }

    if (dark) {
      styleColor = { ...styleColor, ...styles.bgDark }

      if (bordered || clear || link) {
        styleColor = { ...styleColor, ...styles.colorDark }

        if (bordered) {
          styleColor = { ...styleColor, ...styles.borderDark }
        }
      }
    }

    if (info) {
      styleColor = { ...styleColor, ...styles.bgInfo }

      if (bordered || clear || link) {
        styleColor = { ...styleColor, ...styles.colorInfo }

        if (bordered) {
          styleColor = { ...styleColor, ...styles.borderInfo }
        }
      }
    }

    if (light) {
      styleColor = { ...styleColor, ...styles.bgLight, color: 'black' }

      if (bordered || clear || link) {
        styleColor = { ...styleColor, ...styles.colorLight }

        if (bordered) {
          styleColor = { ...styleColor, ...styles.borderLight }
        }
      }
    }

    if (success) {
      styleColor = { ...styleColor, ...styles.bgSuccess }

      if (bordered || clear || link) {
        styleColor = { ...styleColor, ...styles.colorSuccess }

        if (bordered) {
          styleColor = { ...styleColor, ...styles.borderSuccess }
        }
      }
    }

    if (warning) {
      styleColor = { ...styleColor, ...styles.bgWarning }

      if (bordered || clear || link) {
        styleColor = { ...styleColor, ...styles.colorWarning }

        if (bordered) {
          styleColor = { ...styleColor, ...styles.borderWarning }
        }
      }
    }

    if (bordered) {
      styleColor = { ...styleColor, backgroundColor: 'white' }
    }

    if (clear || link) {
      styleColor = { ...styleColor, backgroundColor: 'transparent' }
    }

    const styleBg = this._filterStyle('backgroundColor')
    if (styleBg) {
      styleColor = { ...styleColor, backgroundColor: styleBg }
    }

    const styleClr = this._filterStyle('color')
    if (styleClr) {
      styleColor = { ...styleColor, color: styleClr }
    } else if (!light && !link && !clear) {
      const isLight = tinycolor(styleColor.backgroundColor).getBrightness() > 225
      styleColor = { ...styleColor, color: isLight ? 'black' : 'white' }
    }

    return styleColor
  }

  /**
   * Method to generate the ripple effect for the button component
   * @private
   * @returns {BackgroundPropType}
   */
  private _getBackgroundRipple(): BackgroundPropType {
    const { backgroundRipple, link } = this._processProps()
    const { background } = this.props
    const rippleColor = 'rgba(256, 256, 256, 0.3)'
    const rippleColorDark = 'rgba(0, 0, 0, 0.15)'
    let _bg = TouchableNativeFeedback.Ripple(rippleColor)

    if (background) {
      _bg = background as RippleBackgroundPropType
    } else if (!background && backgroundRipple) {
      _bg = TouchableNativeFeedback.Ripple(backgroundRipple)
    } else {
      const _sBg = this._filterStyle('backgroundColor', this._processStyle())
      const isLight = tinycolor(_sBg).getBrightness() > 225
      _bg = TouchableNativeFeedback.Ripple(isLight || link ? rippleColorDark : rippleColor)
    }

    return _bg
  }

  /**
   * Method that return the last value of a property of a style prop
   * @private
   * @param {string} prop Property to find
   * @param {*} [style] (optional). Style prop to use
   * @returns {*}
   */
  private _filterStyle(prop: string, style?: any): any {
    // tslint:disable-next-line: no-parameter-reassignment
    if (!style) { style = this.props.style }
    let res

    if (Array.isArray(style)) {
      for (const _style of style) {
        if (_style  && typeof _style[prop] !== 'undefined') {
          res = _style[prop]
        }
      }
    } else if (style) {
      res = style[prop]
    }

    return res
  }
}
