import { StyleSheet } from 'react-native'

const colors = {
  danger: '#ce3c3e',
  dark: '#000000',
  info: '#529ff3',
  light: '#f1f1f1',
  primary: '#0a60ff',
  success: '#4dad4a',
  warning: '#eb9e3e',
}

const materialColors = {
  danger: '#d32f2f',
  dark: colors.dark,
  info: '#00acc1',
  light: colors.light,
  primary: '#1976d2',
  success: '#388e3c',
  warning: '#ffb300',
}

const useMaterialColors = true

const styles = StyleSheet.create({
  bgDanger: {
    backgroundColor: useMaterialColors ? materialColors.danger : colors.danger,
  },
  bgDark: {
    backgroundColor: useMaterialColors ? materialColors.dark : colors.dark,
  },
  bgInfo: {
    backgroundColor: useMaterialColors ? materialColors.info : colors.info,
  },
  bgLight: {
    backgroundColor: useMaterialColors ? materialColors.light : colors.light,
  },
  bgPrimary: {
    backgroundColor: useMaterialColors ? materialColors.primary : colors.primary,
  },
  bgSuccess: {
    backgroundColor: useMaterialColors ? materialColors.success : colors.success,
  },
  bgWarning: {
    backgroundColor: useMaterialColors ? materialColors.warning : colors.warning,
  },

  borderDanger: {
    borderColor: useMaterialColors ? materialColors.danger : colors.danger,
  },
  borderDark: {
    borderColor: useMaterialColors ? materialColors.dark : colors.dark,
  },
  borderInfo: {
    borderColor: useMaterialColors ? materialColors.info : colors.info,
  },
  borderLight: {
    borderColor: useMaterialColors ? materialColors.light : colors.light,
  },
  borderPrimary: {
    borderColor: useMaterialColors ? materialColors.primary : colors.primary,
  },
  borderSuccess: {
    borderColor: useMaterialColors ? materialColors.success : colors.success,
  },
  borderWarning: {
    borderColor: useMaterialColors ? materialColors.warning : colors.warning,
  },

  colorDanger: {
    color: useMaterialColors ? materialColors.danger : colors.danger,
  },
  colorDark: {
    color: useMaterialColors ? materialColors.dark : colors.dark,
  },
  colorInfo: {
    color: useMaterialColors ? materialColors.info : colors.info,
  },
  colorLight: {
    color: useMaterialColors ? materialColors.light : colors.light,
  },
  colorPrimary: {
    color: useMaterialColors ? materialColors.primary : colors.primary,
  },
  colorSuccess: {
    color: useMaterialColors ? materialColors.success : colors.success,
  },
  colorWarning: {
    color: useMaterialColors ? materialColors.warning : colors.warning,
  },

  titleStyle: {
  },

  btnLink: {
    height: 'auto',
    paddingBottom: 1,
    paddingTop: 0,
  },

  titleLink: {
    fontSize: 16,
    paddingLeft: 4,
    paddingRight: 4,
    textDecorationLine: 'underline',
    textDecorationStyle: 'solid',
  },

  center: {
    alignSelf: 'center',
  },

  left: {
    alignSelf: 'flex-start',
  },

  right: {
    alignSelf: 'flex-end',
  },

})

export default styles
