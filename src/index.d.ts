import * as React from 'react'
import {
  BackgroundPropType,
  RecursiveArray,
  RegisteredStyle,
  StyleProp,
  TextInputProps,
  TouchableNativeFeedbackProps,
  TouchableOpacity,
  TouchableWithoutFeedbackProps,
  ViewStyle,
} from 'react-native'

import { TypeComponent, TypeStyle } from '@ticmakers-react-native/core'
import { IIconProps } from '@ticmakers-react-native/icon'

/**
 * Interface to define the touchable opacity props of the Button component
 * @interface ITouchableOpacityProps
 * @extends {TouchableWithoutFeedbackProps}
 * @platform iOS
 */
export interface ITouchableOpacityProps extends TouchableWithoutFeedbackProps {
  /**
   * Determines what the opacity of the wrapped view should be when touch is active
   * @type {number}
   * @default 0.2
   */
  activityOpacity?: number

  /**
   * (Apple TV only) TV preferred focus (see documentation for the View component)
   * @type {boolean}
   * @platform iOS
   */
  hasTVPreferredFocus?: boolean

  /**
   * Apply a custom style
   * @type {TypeStyle}
   */
  style?: TypeStyle,

  /**
   * (Apple TV only) Object with properties to control Apple TV parallax effects.
   *
   * enabled: If true, parallax effects are enabled. Defaults to true. shiftDistanceX: Defaults to 2.0. shiftDistanceY: Defaults to 2.0. tiltAngle: Defaults to 0.05. magnification: Defaults to 1.0. pressMagnification: Defaults to 1.0. pressDuration: Defaults to 0.3. pressDelay: Defaults to 0.0.
   * @type {object}
   * @platform iOS
   */
  tvParallaxProperties?: object
}

/**
 * Interface to define the props of the Button component
 * @interface IButtonProps
 */
export interface IButtonProps extends TouchableNativeFeedbackProps {
  /**
   * State of the button
   * @type {boolean}
   * @default false
   */
  active?: boolean

  /**
   * Background ripple effect
   * @type {BackgroundPropType}
   */
  background?: BackgroundPropType

  /**
   * Apply a color to the background ripple
   * @type {string}
   */
  backgroundRipple?: string

  /**
   * Block level button
   * @type {boolean}
   * @default false
   */
  block?: boolean

  /**
   * Applies outline button style
   * @type {boolean}
   * @default false
   */
  bordered?: boolean

  /**
   * Align the button to the center
   * @type {boolean}
   * @default false
   */
  center?: boolean

  /**
   * Renders child element of button
   * @type {boolean}
   * @default false
   */
  clear?: boolean

  /**
   * Red background color for button
   * @type {boolean}
   * @default false
   */
  danger?: boolean

  /**
   * Black background color for button
   * @type {boolean}
   * @default false
   */
  dark?: boolean

  /**
   * Disables click option for button
   * @type {boolean}
   * @default false
   */
  disabled?: boolean

  /**
   * Full width button
   * @type {boolean}
   * @default false
   */
  full?: boolean

  /**
   * Hide all icons in the button
   * @type {boolean}
   * @default false
   */
  hideIcons?: boolean

  /**
   * Hide the icon left in the button
   * @type {boolean}
   * @default false
   */
  hideIconLeft?: boolean

  /**
   * Hide the icon right in the button
   * @type {boolean}
   * @default false
   */
  hideIconRight?: boolean

  /**
   * Left padding for the icon
   * @type {(IIconProps | TypeComponent)}
   */
  iconLeft?: IIconProps | TypeComponent | boolean

  /**
   * Right padding for the icon
   * @type {(IIconProps | TypeComponent)}
   */
  iconRight?: IIconProps | TypeComponent | boolean

  /**
   * Light blue background color for button
   * @type {boolean}
   * @default false
   */
  info?: boolean

  /**
   * Large size button
   * @type {boolean}
   * @default false
   */
  large?: boolean

  /**
   * Align the button to the left
   * @type {boolean}
   * @default false
   */
  left?: boolean

  /**
   * Light white background color for button
   * @type {boolean}
   * @default false
   */
  light?: boolean

  /**
   * Apply a style as link url
   * @type {boolean}
   * @default false
   */
  link?: boolean

  /**
   * Disable the button and add a spinner
   * @type {boolean}
   */
  loading?: boolean

  /**
   * Method thar fire when the button is pressed
   */
  onPress?: () => void

  /**
   * Blue background color for button
   * @type {boolean}
   * @default true
   */
  primary?: boolean

  /**
   * Align the button to the right
   * @type {boolean}
   * @default false
   */
  right?: boolean

  /**
   * Renders button with slightly round shaped edges
   * @type {boolean}
   * @default false
   */
  rounded?: boolean

  /**
   * Small size button
   * @type {boolean}
   * @default false
   */
  small?: boolean

  /**
   * Green background color for button
   * @type {boolean}
   * @default false
   */
  success?: boolean

  /**
   * Apply a custom style to the button
   * @type {TypeStyle}
   */
  style?: TypeStyle

  /**
   * Define a title for button
   * @type {string}
   */
  title?: string

  /**
   * Apply a custom style to the title for button
   * @type {TypeStyle}
   */
  titleStyle?: TypeStyle & { color: string }

  /**
   * Yellow background color for button
   * @type {boolean}
   * @default false
   */
  warning?: boolean
}

/**
 * Interface to define the state of the Button component
 * @interface IButtonState
 */
export interface IButtonState {
}

/**
 * Class to define the component Button
 * @class Button
 * @extends {React.Component<IButtonProps, IButtonState>}
 */
declare class Button extends React.Component<IButtonProps, IButtonState> {
  /**
   * Method that renders the component
   * @returns {TypeComponent}
   */
  public render(): TypeComponent

  /**
   * Method that return a array of components for the button content
   * @returns {TypeComponent[]}
   */
  public ButtonContent(): TypeComponent[]

  /**
   * Method thar return the title component
   * @returns {TypeComponent}
   */
  public Title(): TypeComponent

  /**
   * Method that return the icon left component
   * @returns {TypeComponent}
   */
  public IconLeft(): TypeComponent

  /**
   * Method that return the icon right component
   * @returns {TypeComponent}
   */
  public IconRight(): TypeComponent

  /**
   * Method that fire when the button is pressed
   * @private
   * @returns {void}
   */
  private _onPressAction(): void

  /**
   * Method that valid if a object is a props
   * @private
   * @param {*} kind Object to validate
   * @returns {boolean}
   */
  private _isProps(kind: any): boolean

  /**
   * Method that return the children components
   * @private
   * @returns {TypeComponent[]}
   */
  private _getChildren(): TypeComponent[]

  /**
   * Method that process the props of the button component
   * @private
   * @returns {IButtonProps}
   */
  private _processProps(): IButtonProps

  /**
   * Method that process the style prop of the button component
   * @private
   * @returns {TypeStyle}
   */
  private _processStyle(): TypeStyle

  /**
   * Method to generate the style theme for the button component
   * @private
   * @returns {TypeStyle}
   */
  private _getStyleColor(): TypeStyle

  /**
   * Method to generate the ripple effect for the button component
   * @private
   * @returns {BackgroundPropType}
   */
  private _getBackgroundRipple(): BackgroundPropType

  /**
   * Method that return the last value of a property of a style prop
   * @private
   * @param {string} prop Property to find
   * @param {*} [style] (optional). Style prop to use
   * @returns {*}
   */
  private _filterStyle(prop: string, style?: any): any
}

/**
 * Export default
 */
export default Button
