# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Released

## [1.0.5] - 2019-10-09

### Fixed

- Fixed style props issue
- Fixed typings

## [1.0.4] - 2019-09-24

### Fixed

- Fixed dependencies version

## [1.0.3] - 2019-05-02

### Added

- Implement new helper to validate the components

## [1.0.2] - 2019-04-19

### Fixed

- Fixed style bordered and clear

## [1.0.1] - 2019-04-15

### Fixed

- Fixed styles button

## [1.0.0] - 2019-04-07

### Release

# Unreleased

## [1.0.0-beta.2] - 2019-04-07

### Fixed

- Fix import tinycolor2

## [1.0.0-beta.1] - 2019-04-07

### Fixed

- Fix import lodash

## [1.0.0-beta.0] - 2019-04-07

### Added

- Upload and publish first version

[1.0.5]: https://bitbucket.org/ticmakers/rn-button/src/v1.0.5/
[1.0.4]: https://bitbucket.org/ticmakers/rn-button/src/v1.0.4/
[1.0.3]: https://bitbucket.org/ticmakers/rn-button/src/v1.0.3/
[1.0.2]: https://bitbucket.org/ticmakers/rn-button/src/v1.0.2/
[1.0.1]: https://bitbucket.org/ticmakers/rn-button/src/v1.0.1/
[1.0.0]: https://bitbucket.org/ticmakers/rn-button/src/v1.0.0/
[1.0.0-beta.2]: #
[1.0.0-beta.1]: #
[1.0.0-beta.0]: #
