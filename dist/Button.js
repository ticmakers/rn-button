"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var React = require("react");
var native_base_1 = require("native-base");
var tinycolor = require("tinycolor2");
var _ = require("lodash");
var react_native_1 = require("react-native");
var core_1 = require("@ticmakers-react-native/core");
var icon_1 = require("@ticmakers-react-native/icon");
var styles_1 = require("./styles");
var Button = (function (_super) {
    __extends(Button, _super);
    function Button(props) {
        var _this = _super.call(this, props) || this;
        _this.state = _this._processProps();
        _this._getBackgroundRipple();
        return _this;
    }
    Button.prototype.render = function () {
        var _a = this._processProps(), clear = _a.clear, disabled = _a.disabled, iconLeft = _a.iconLeft, iconRight = _a.iconRight, link = _a.link, loading = _a.loading;
        var props = __assign(__assign(__assign({}, this.props), this._processProps()), { background: this._getBackgroundRipple(), center: undefined, disabled: disabled || loading, iconLeft: iconLeft, iconRight: iconRight, left: undefined, onPress: this._onPressAction.bind(this), right: undefined, style: this._processStyle(), transparent: clear || link });
        return (React.createElement(native_base_1.Button, __assign({}, props), this.ButtonContent()));
    };
    Button.prototype.ButtonContent = function () {
        var _a = this._processProps(), hideIconLeft = _a.hideIconLeft, hideIconRight = _a.hideIconRight, hideIcons = _a.hideIcons, iconLeft = _a.iconLeft, iconRight = _a.iconRight, large = _a.large, loading = _a.loading, small = _a.small, title = _a.title;
        if (loading) {
            var _props = {
                color: this._filterStyle('color', this._processStyle()),
                key: 'loading',
                size: (small || loading) ? 'small' : (large ? 'large' : 26),
            };
            return [React.createElement(react_native_1.ActivityIndicator, __assign({}, _props))];
        }
        return ([
            (iconLeft && !hideIcons && !hideIconLeft) && this.IconLeft(),
            this._getChildren() || (title && this.Title()),
            (iconRight && !hideIcons && !hideIconRight) && this.IconRight(),
        ]);
    };
    Button.prototype.Title = function () {
        var _a = this._processProps(), link = _a.link, title = _a.title, titleStyle = _a.titleStyle;
        var _color = this._getStyleColor().color;
        var _style = { color: _color };
        return (React.createElement(native_base_1.Text, { key: title, style: [styles_1.default.titleStyle, _style, link && styles_1.default.titleLink, titleStyle], uppercase: !link }, title));
    };
    Button.prototype.IconLeft = function () {
        var iconLeft = this._processProps().iconLeft;
        var icon;
        var _style = this._getStyleColor();
        var _color = _style.color;
        if (iconLeft) {
            if (core_1.AppHelper.isComponent(iconLeft)) {
                var color = iconLeft.props.color;
                icon = React.cloneElement(iconLeft, { color: (color || _color) });
            }
            else if (this._isProps(iconLeft)) {
                icon = React.createElement(icon_1.default, __assign({ color: _color }, iconLeft, { key: 0 }));
            }
        }
        return icon;
    };
    Button.prototype.IconRight = function () {
        var iconRight = this._processProps().iconRight;
        var icon;
        var _style = this._getStyleColor();
        var _color = _style.color;
        if (iconRight) {
            if (core_1.AppHelper.isComponent(iconRight)) {
                var color = iconRight.props.color;
                icon = React.cloneElement(iconRight, { color: (color || _color) });
            }
            else if (this._isProps(iconRight)) {
                icon = React.createElement(icon_1.default, __assign({ color: _color }, iconRight, { key: 1 }));
            }
        }
        return icon;
    };
    Button.prototype._onPressAction = function () {
        var onPress = this._processProps().onPress;
        if (onPress) {
            return onPress();
        }
    };
    Button.prototype._isProps = function (kind) {
        return (!core_1.AppHelper.isComponent(kind) && Object.keys(kind).length > 0);
    };
    Button.prototype._getChildren = function () {
        var _a = this._processProps(), hideIcons = _a.hideIcons, hideIconLeft = _a.hideIconLeft, hideIconRight = _a.hideIconRight, link = _a.link;
        var children = this.props.children;
        var _style = this._getStyleColor();
        var _color = _style.color;
        return React.Children.map(children, function (child, index) {
            var isIcon = child.type === icon_1.default;
            var isText = child.type === native_base_1.Text;
            var isIconLeft = (isIcon && index === 0);
            var isIconRight = (isIcon && index >= 1);
            var propsIcon = __assign({ color: _color }, child.props);
            var propsText = __assign({ style: { color: _color } }, child.props);
            if (link) {
                propsText.style = __assign(__assign({}, propsText.style), styles_1.default.titleLink);
                propsText.uppercase = false;
            }
            if (isIcon && !hideIcons) {
                if ((isIconLeft && !hideIconLeft) || (isIconRight && !hideIconRight)) {
                    return React.cloneElement(child, propsIcon);
                }
            }
            else if (!isIcon) {
                return React.cloneElement(child, isText ? propsText : child.props);
            }
        });
    };
    Button.prototype._processProps = function () {
        var _a = this.props, active = _a.active, backgroundRipple = _a.backgroundRipple, block = _a.block, bordered = _a.bordered, center = _a.center, clear = _a.clear, danger = _a.danger, dark = _a.dark, disabled = _a.disabled, full = _a.full, hideIconLeft = _a.hideIconLeft, hideIconRight = _a.hideIconRight, hideIcons = _a.hideIcons, iconLeft = _a.iconLeft, iconRight = _a.iconRight, info = _a.info, large = _a.large, left = _a.left, light = _a.light, link = _a.link, loading = _a.loading, onPress = _a.onPress, primary = _a.primary, right = _a.right, rounded = _a.rounded, small = _a.small, style = _a.style, success = _a.success, title = _a.title, titleStyle = _a.titleStyle, warning = _a.warning;
        var props = {
            active: (typeof active !== 'undefined' ? active : false),
            backgroundRipple: (typeof backgroundRipple !== 'undefined' ? backgroundRipple : undefined),
            block: (typeof block !== 'undefined' ? block : false),
            bordered: (typeof bordered !== 'undefined' ? bordered : false),
            center: (typeof center !== 'undefined' ? center : false),
            clear: (typeof clear !== 'undefined' ? clear : false),
            danger: (typeof danger !== 'undefined' ? danger : false),
            dark: (typeof dark !== 'undefined' ? dark : false),
            disabled: (typeof disabled !== 'undefined' ? disabled : false),
            full: (typeof full !== 'undefined' ? full : false),
            hideIconLeft: (typeof hideIconLeft !== 'undefined' ? hideIconLeft : false),
            hideIconRight: (typeof hideIconRight !== 'undefined' ? hideIconRight : false),
            hideIcons: (typeof hideIcons !== 'undefined' ? hideIcons : false),
            iconLeft: (typeof iconLeft !== 'undefined' ? iconLeft : false),
            iconRight: (typeof iconRight !== 'undefined' ? iconRight : false),
            info: (typeof info !== 'undefined' ? info : false),
            large: (typeof large !== 'undefined' ? large : false),
            left: (typeof left !== 'undefined' ? left : false),
            light: (typeof light !== 'undefined' ? light : false),
            link: (typeof link !== 'undefined' ? link : false),
            loading: (typeof loading !== 'undefined' ? loading : false),
            onPress: (typeof onPress !== 'undefined' ? onPress : undefined),
            primary: (typeof primary !== 'undefined' ? primary : true),
            right: (typeof right !== 'undefined' ? right : false),
            rounded: (typeof rounded !== 'undefined' ? rounded : false),
            small: (typeof small !== 'undefined' ? small : false),
            style: (typeof style !== 'undefined' ? style : undefined),
            success: (typeof success !== 'undefined' ? success : false),
            title: (typeof title !== 'undefined' ? title : undefined),
            titleStyle: (typeof titleStyle !== 'undefined' ? titleStyle : undefined),
            warning: (typeof warning !== 'undefined' ? warning : false),
        };
        return props;
    };
    Button.prototype._processStyle = function () {
        var _a = this._processProps(), center = _a.center, iconLeft = _a.iconLeft, iconRight = _a.iconRight, left = _a.left, link = _a.link, right = _a.right, style = _a.style;
        var _style = _.clone(style);
        var _newStyle = __assign({}, this._getStyleColor());
        if (iconLeft) {
            _newStyle.paddingLeft = 16;
        }
        if (iconRight) {
            _newStyle.paddingRight = 16;
        }
        if (center) {
            _newStyle.alignSelf = 'center';
        }
        if (left) {
            _newStyle.alignSelf = 'flex-start';
        }
        if (right) {
            _newStyle.alignSelf = 'flex-end';
        }
        if (Array.isArray(_style)) {
            _style.unshift(_newStyle);
            if (link) {
                _style.unshift(styles_1.default.btnLink);
            }
        }
        else if (typeof _style === 'object' && Object.keys(_style).length > 0) {
            _style = react_native_1.StyleSheet.flatten([_newStyle, _style]);
            if (link) {
                _style = react_native_1.StyleSheet.flatten([styles_1.default.btnLink, _style]);
            }
        }
        if (!_style) {
            _newStyle = react_native_1.StyleSheet.flatten([_newStyle, styles_1.default.btnLink]);
        }
        return _style || _newStyle;
    };
    Button.prototype._getStyleColor = function () {
        var _a = this._processProps(), bordered = _a.bordered, clear = _a.clear, danger = _a.danger, dark = _a.dark, info = _a.info, light = _a.light, link = _a.link, success = _a.success, warning = _a.warning;
        var styleColor = __assign(__assign({}, styles_1.default.bgPrimary), { color: 'white' });
        if (bordered || clear || link) {
            styleColor = __assign(__assign({}, styleColor), styles_1.default.colorPrimary);
            if (bordered) {
                styleColor = __assign(__assign({}, styleColor), styles_1.default.borderPrimary);
            }
        }
        if (danger) {
            styleColor = __assign(__assign({}, styleColor), styles_1.default.bgDanger);
            if (bordered || clear || link) {
                styleColor = __assign(__assign({}, styleColor), styles_1.default.colorDanger);
                if (bordered) {
                    styleColor = __assign(__assign({}, styleColor), styles_1.default.borderDanger);
                }
            }
        }
        if (dark) {
            styleColor = __assign(__assign({}, styleColor), styles_1.default.bgDark);
            if (bordered || clear || link) {
                styleColor = __assign(__assign({}, styleColor), styles_1.default.colorDark);
                if (bordered) {
                    styleColor = __assign(__assign({}, styleColor), styles_1.default.borderDark);
                }
            }
        }
        if (info) {
            styleColor = __assign(__assign({}, styleColor), styles_1.default.bgInfo);
            if (bordered || clear || link) {
                styleColor = __assign(__assign({}, styleColor), styles_1.default.colorInfo);
                if (bordered) {
                    styleColor = __assign(__assign({}, styleColor), styles_1.default.borderInfo);
                }
            }
        }
        if (light) {
            styleColor = __assign(__assign(__assign({}, styleColor), styles_1.default.bgLight), { color: 'black' });
            if (bordered || clear || link) {
                styleColor = __assign(__assign({}, styleColor), styles_1.default.colorLight);
                if (bordered) {
                    styleColor = __assign(__assign({}, styleColor), styles_1.default.borderLight);
                }
            }
        }
        if (success) {
            styleColor = __assign(__assign({}, styleColor), styles_1.default.bgSuccess);
            if (bordered || clear || link) {
                styleColor = __assign(__assign({}, styleColor), styles_1.default.colorSuccess);
                if (bordered) {
                    styleColor = __assign(__assign({}, styleColor), styles_1.default.borderSuccess);
                }
            }
        }
        if (warning) {
            styleColor = __assign(__assign({}, styleColor), styles_1.default.bgWarning);
            if (bordered || clear || link) {
                styleColor = __assign(__assign({}, styleColor), styles_1.default.colorWarning);
                if (bordered) {
                    styleColor = __assign(__assign({}, styleColor), styles_1.default.borderWarning);
                }
            }
        }
        if (bordered) {
            styleColor = __assign(__assign({}, styleColor), { backgroundColor: 'white' });
        }
        if (clear || link) {
            styleColor = __assign(__assign({}, styleColor), { backgroundColor: 'transparent' });
        }
        var styleBg = this._filterStyle('backgroundColor');
        if (styleBg) {
            styleColor = __assign(__assign({}, styleColor), { backgroundColor: styleBg });
        }
        var styleClr = this._filterStyle('color');
        if (styleClr) {
            styleColor = __assign(__assign({}, styleColor), { color: styleClr });
        }
        else if (!light && !link && !clear) {
            var isLight = tinycolor(styleColor.backgroundColor).getBrightness() > 225;
            styleColor = __assign(__assign({}, styleColor), { color: isLight ? 'black' : 'white' });
        }
        return styleColor;
    };
    Button.prototype._getBackgroundRipple = function () {
        var _a = this._processProps(), backgroundRipple = _a.backgroundRipple, link = _a.link;
        var background = this.props.background;
        var rippleColor = 'rgba(256, 256, 256, 0.3)';
        var rippleColorDark = 'rgba(0, 0, 0, 0.15)';
        var _bg = react_native_1.TouchableNativeFeedback.Ripple(rippleColor);
        if (background) {
            _bg = background;
        }
        else if (!background && backgroundRipple) {
            _bg = react_native_1.TouchableNativeFeedback.Ripple(backgroundRipple);
        }
        else {
            var _sBg = this._filterStyle('backgroundColor', this._processStyle());
            var isLight = tinycolor(_sBg).getBrightness() > 225;
            _bg = react_native_1.TouchableNativeFeedback.Ripple(isLight || link ? rippleColorDark : rippleColor);
        }
        return _bg;
    };
    Button.prototype._filterStyle = function (prop, style) {
        if (!style) {
            style = this.props.style;
        }
        var res;
        if (Array.isArray(style)) {
            for (var _i = 0, style_1 = style; _i < style_1.length; _i++) {
                var _style = style_1[_i];
                if (_style && typeof _style[prop] !== 'undefined') {
                    res = _style[prop];
                }
            }
        }
        else if (style) {
            res = style[prop];
        }
        return res;
    };
    return Button;
}(React.Component));
exports.default = Button;
//# sourceMappingURL=Button.js.map